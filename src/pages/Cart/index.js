import React from 'react'
import { Link, useHistory } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'

import { createOrder } from '../../store/order'
import { clearCart } from '../../store/cart'
import Header from '../../components/Header'
import Product from '../../components/Product'
import api from '../../services/api'

import { BsArrowBarLeft } from 'react-icons/bs'
import cartImage from '../../assets/images/empt-shopping-cart.jpeg'
import './styles.css'

export default function Cart () {
  const history = useHistory()
  const dispatch = useDispatch()

  const cart = useSelector((state) => state.cart)

  const total = useSelector((state) =>
    state.cart.reduce((total, product) => {
      return total + product.value * product.amount
    }, 0)
  )

  const login = useSelector((state) => state.login)

  function handleCreditBalance (e) {
    const userToken = login[0]
    api
      .get('/user', {
        headers: {
          Authorization: userToken
        }
      })
      .then((response) => {
        const creditBalance = response.data.wallet

        if (total <= creditBalance) {
          handleShop()
        } else {
          alert('Que pena, você está sem saldo para esta compra')
        }
      })
      .catch((error) => {
        const status = error.response.status
        const eMessage = error.response.data

        if (status === 500 && eMessage === 'JsonWebTokenError') {
          alert('Você precisa fazer login antes de comprar!')
        }
      })
  }

  function handleShop (e) {
    const userToken = login[0]

    const productShop = cart.map((element) => ({
      product_id: element.id,
      amount: element.amount
    }))

    api
      .post(
        '/purchase',
        {
          products: productShop
        },
        {
          headers: {
            Authorization: userToken
          }
        }
      )
      .then((res) => {
        const product = res.data
        shopping(product)
        alert('compra realizada com sucesso!')
        dispatch(clearCart())
        history.push('/meu-pedido')
      })
      .catch((error) => {
        console.log(error)
        console.log('deu ruim')
      })
  }

  function shopping (product) {
    dispatch(createOrder(product))
  }

  return (
    <div>
      {cart.length === 0 ? (
        <div className='emptCart'>
          <div className='empt-cart-header'>
            <h1>SEU CARRINHO ESTÁ VAZIO</h1>
            <Link className='btn-back' to='/lista-de-produtos'>
              <BsArrowBarLeft />
              Voltar para o catálogo
            </Link>
          </div>
          <div className='empt-cart-img'>
            <img
              src={cartImage}
              alt='mulher com um carrinho de compras vazio'
            />
          </div>
        </div>
      ) : (
        <>
          <div className='container-cart'>
            <Header />
            <div className='resume'>
              <h2>
                TOTAL:
                {Intl.NumberFormat('pt-br', {
                  style: 'currency',
                  currency: 'BRL'
                }).format(total)}
              </h2>
              <div className='btns'>
                <button className='btn-resume'>
                  <Link to='/lista-de-produtos'>COMPRAR MAIS</Link>
                </button>
                <button
                  className='btn-resume'
                  onClick={() => handleCreditBalance()}
                >
                PAGAR COM SALDO
                </button>
              </div>
            </div>
            <div className='product-cards-cart'>
              {cart.map((product) => (
                <div className='product-cards' key={product.id}>
                  <Product product={product} />
                </div>
              ))}
            </div>
          </div>
        </>
      )}
    </div>
  )
}
