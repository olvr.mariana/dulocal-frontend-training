import React, { useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'

import { login } from '../../store/login'
import api from '../../services/api'

import * as ReactBootStrap from 'react-bootstrap'
import './styles.css'

export default function Login () {
  const dispatch = useDispatch()
  const history = useHistory()
  const { register, handleSubmit, errors } = useForm()

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [loading, setLoading] = useState('')

  async function handleLogin (e) {
    if (email.length > 4 && password.length >= 6) {
      try {
        setLoading(true)
        const response = await api.post('/user/login', { email, password })

        alert('Bem-vindo! Você foi logado!')
        const token = response.headers.authorization
        dispatch(login(token))

        history.push('/lista-de-produtos')
      } catch (error) {
        const status = error.response.status
        const eMessage = error.response.data

        if (status === 401 && eMessage === 'Usuário não encontrado.') {
          alert('Usuário não encontrado! Cadastre-se!')
        } else if (status === 401 && eMessage === 'Senha incorreta.') {
          alert('Senha incorreta. Digite novamente!')
        } else {
          alert('Falha no login, tente novamente')
        }
      } finally {
        setLoading(false)
      }
    }
  }

  return (
    <div className='login-container'>
      <h1>Olá, que bom te ver de novo!</h1>
      <section className='login-form'>
        <h2>Faça seu login:</h2>

        <form onSubmit={handleSubmit(handleLogin)}>
          <input
            id='user-email'
            type='email'
            name='email'
            placeholder='E-mail'
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            ref={register({
              required: 'Por favor, informe o seu e-mail cadastrado',
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                message: 'Formato de e-mail inválido'
              }
            })}
          />
          {errors.email && <p>{errors.email.message}</p>}

          <input
            type='password'
            name='password'
            placeholder='Senha'
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            ref={register({
              required: 'Por favor, coloque sua senha'
            })}
          />
          {errors.password && <p>{errors.password.message}</p>}

          <button type='submit' className='button' disable={loading}>
            {loading && <ReactBootStrap.Spinner animation='border' />}
            Entrar
          </button>
        </form>

        <Link to='/cadastro'>Não tenho cadastro</Link>
      </section>
    </div>
  )
}
