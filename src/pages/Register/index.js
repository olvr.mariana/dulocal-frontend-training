import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { validate } from 'gerador-validador-cpf'
import InputMask from 'react-input-mask'

import { login } from '../../store/login'
import api from '../../services/api'

import * as ReactBootStrap from 'react-bootstrap'
import './styles.css'

export default function Register () {
  const history = useHistory()
  const dispatch = useDispatch()

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [date, setDate] = useState('')
  const [cpf, setCpf] = useState('')
  const [loading, setLoading] = useState(false)

  const { register, handleSubmit, errors } = useForm()

  function loginAuto () {
    api.post('/user/login', { email, password }).then((res) => {
      const token = res.headers.authorization
      dispatch(login(token))

      setLoading(false)
      history.push('/lista-de-produtos')
    })
  }

  async function handleRegister (e) {
    const birthDate = date
    const yBirth = birthDate.substring(6)

    const cpfNumber = cpf
    const validacao = validate(cpfNumber)

    const data = {
      email,
      password
    }

    if (validacao === false) {
      return alert('Ops, cpf inválido!')
    } else if (yBirth > 2020) {
      return alert('Data de nascimento inválida! Digite novamente')
    } else if (email.length > 4 && password.length >= 6) {
    }
    try {
      setLoading(true)
      const response = await api.post('/user', data)

      alert(`Olá, ${response.data.email}. Bem-vindo à lujinha!`)

      loginAuto()
    } catch (error) {
      const status = error.response.status
      const eMessage = error.response.data.error
      const message = error.response.data.errorMessage

      if (
        status === 400 &&
        message === 'length must be at least 6 characters long'
      ) {
        alert('A senha deve ter no mínimo 6 caracteres. Tente novamente!')
      } else if (status === 409 && eMessage === 'UniqueViolation') {
        alert('O e-mail já está cadastrado! Tente novamente')
      } else {
        alert('Tente novamente!')
      }
    }
  }

  return (
    <div className='register-container'>
      <h1>CADASTRE-SE PARA COMPRAR</h1>

      <form onSubmit={handleSubmit(handleRegister)}>
        <label>E-mail</label>
        <input
          name='email'
          id='inputEmail'
          ref={register({
            required: 'Por favor, digite um e-mail!',
            pattern: {
              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
              message: 'E-mail inválido'
            }
          })}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        {errors.email && <p class='error'>{errors.email.message}</p>}

        <label>Senha</label>
        <input
          type='password'
          name='password'
          id='inputPassword'
          ref={register({
            required: 'Por favor, digite uma senha!',
            minLength: {
              value: 6,
              message: 'A senha deve ter no mínimo 6 dígitos!'
            }
          })}
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        {errors.password && <p class='error'>{errors.password.message}</p>}

        <label>Data de nascimento</label>

        <InputMask
          mask='99/99/9999'
          placeholder='dd/mm/aaaa'
          ref={register({
            required: 'Informe sua data de nascimento!',
            minLength: {
              value: 10,
              message: 'Por favor, insira a data com as barras de separação'
            },
            maxLength: {
              value: 10,
              message: 'Ops, data incorreta!'
            },
            pattern: {
              value: /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/i,
              message: 'Data inválida'
            }
          })}
          value={date}
          onChange={(e) => setDate(e.target.value)}
        />

        {errors.date && <p class='error'>{errors.date.message}</p>}

        <label>CPF</label>
        <InputMask
          mask='999.999.999-99'
          placeholder='000.000.000-00'
          ref={register({
            required: 'Por favor, informe o seu CPF!',
            minLength: {
              value: 11,
              message:
                'Informe o CPF com 11 dígitos, pontos e traços são opcionais'
            },
            maxLength: {
              value: 14,
              message:
                'Informe o CPF com 11 dígitos, pontos e traços são opcionais.'
            },
            pattern: {
              value: /^[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}$/,
              message: 'Use apenas números para informar o CPF!'
            }
          })}
          value={cpf}
          onChange={(e) => setCpf(e.target.value)}
        />
        {errors.cpf && <p class='error'>{errors.cpf.message}</p>}

        <button type='submit' className='button' disable={loading}>
          {loading && <ReactBootStrap.Spinner animation='border' />}
          Cadastrar
        </button>
      </form>
    </div>
  )
}
