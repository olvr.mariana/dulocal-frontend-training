import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import queryString from 'query-string'

import api from '../../services/api'
import './styles.css'

export default function OrderHistory () {
  const login = useSelector((state) => state.login)
  const [orders, setOrders] = useState([])
  const [orderedProducts, setOrderedProducts] = useState([])
  const [products, setProducts] = useState([])
  const [pages, setPages] = useState()

  useEffect(() => {
    getPurchases()
  }, [])

  useEffect(() => {
    getProductNames()
  }, [pages])

  function getPurchases () {
    const userToken = login[0]
    api
      .get('/purchase/user', { headers: { Authorization: userToken } })
      .then((response) => {
        const purchaseData = response.data
        const purchaseDataReverse = purchaseData.reverse()
        setOrders(purchaseDataReverse)

        const produtos = purchaseDataReverse
          .map((compra) => compra.products)
          .reduce((produtos, items) => {
            return produtos.concat(
              items.filter((item) => {
                return produtos.map((item) => item.id).indexOf(item.id) === -1
              })
            )
          }, [])
        setOrderedProducts(produtos)
      })
      .catch((error) => {
        console.error(error)
      })
  }

  function getProductNames () {
    const query = queryString.stringifyUrl({
      url: '/product',
      query: {
        page: pages
      }
    })

    api
      .get(query)
      .then((response) => {
        const limitPerPage = 20
        const total = response.data.total
        const totalPages = Math.ceil(total / limitPerPage)
        const arrayPages = []
        for (let i = 0; i < totalPages; i++) {
          arrayPages.push(i)
        }
        setPages(arrayPages)
        setProducts(response.data.results)
      })
      .catch((error) => {
        console.error(error)
      })
  }
  return (
    <div className='container'>
      <div className='header-orders'>
        <h1>Minhas compras</h1>
      </div>
      <div className='cards-orders'>
        {orders.map((pedido) => (
          <div className='card-orders' key={pedido.id}>
            <h5>
              <strong>Número do pedido: </strong>
              {pedido.id}
            </h5>

            {orderedProducts.map((item) => {
              if (pedido.id === item.purchase_id) {
                return (
                  <div className='card-orders-products' key={item.id}>
                    {products.map((product) => {
                      if (product.id === item.product_id) {
                        return (
                          <p>
                            <strong>{product.name}</strong>
                          </p>
                        )
                      }
                    })}
                    <p>
                      <strong>Preço: </strong>
                      {Intl.NumberFormat('pt-br', {
                        style: 'currency',
                        currency: 'BRL'
                      }).format(item.value)}
                    </p>
                    <p>
                      <strong>Quantidade: </strong>
                      {item.amount}
                    </p>

                    <p>
                      <strong>Total: </strong>
                      {Intl.NumberFormat('pt-br', {
                        style: 'currency',
                        currency: 'BRL'
                      }).format(item.amount * item.value)}
                    </p>
                  </div>
                )
              }
            })}
          </div>
        ))}
      </div>

      <div className='btns-purchase'>
        <Link to='/lista-de-produtos'>
          <button className='btn btn-purchase'>Catálogo de produtos</button>
        </Link>
      </div>
    </div>
  )
}
