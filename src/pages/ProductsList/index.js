import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import queryString from 'query-string'

import Header from '../../components/Header'
import Product from '../../components/Product'
import api from '../../services/api'

import { FcSearch } from 'react-icons/fc'
import './styles.css'

export default function ProductList () {
  const [products, setProducts] = useState([])
  const [images, setImages] = useState([])
  const [nameSearch, setNameSearch] = useState('')
  const [pages, setPages] = useState([])
  const [currentPage, setCurrentPage] = useState([0])

  useEffect(() => {
    getAllProducts()
  }, [nameSearch, currentPage])

  function getAllProducts () {
    const query = queryString.stringifyUrl({
      url: '/product',
      query: {
        page: currentPage,
        ...(nameSearch ? { name: nameSearch } : {}),
        'amount[min]': 1,
        limit: 20
      }
    })

    api.get(query).then((response) => {
      const limitPerPage = 20
      const total = response.data.total
      const totalPages = Math.ceil(total / limitPerPage)
      const arrayPages = []
      for (let i = 0; i < totalPages; i++) {
        arrayPages.push(i)
      }

      setPages(arrayPages)
      setProducts(response.data.results)

      const getPicture = async (id) => {
        try {
          const response = await api.get(`/product/${id}/image`)
          return {
            productId: id,
            image: response.data
          }
        } catch (err) {
          return {
            productId: id,
            image: null
          }
        }
      }
      Promise.all(
        response.data.results.map((item) => getPicture(item.id))
      ).then((response) => {
        setImages(response)
      })
    })
  }

  function getImage (productId) {
    const objectFounded = images.find((item) => item.productId === productId)
    if (objectFounded && objectFounded.image) {
      return objectFounded.image
    }
    return ''
  }

  return (
    <div>
      <Header />
      <div className='products-container'>
        <p>Olá. Seja bem-vindo!</p>
        <br />

        <div className='search-bar'>
          <div className='search'>
            <p>PROCURANDO POR UM PRODUTO ESPECÍFICO?</p>
            <input
              value={nameSearch}
              onChange={(e) => setNameSearch(e.target.value)}
            />
            <FcSearch />
          </div>

          <div className='pages'>
            {pages.map((page) => (
              <button
                className='btn'
                key={page}
                onClick={() => setCurrentPage(page)}
              >
                {page}
              </button>
            ))}
          </div>
        </div>

        <br />

        <div className='container'>
          {products.map((product) => (
            <div className='product-card' key={product.id}>
              <img src={getImage(product.id)} alt='' />
              <Product product={product} />
              <Link className='btn btn-shop' to='/carrinho-de-compras'>
                Fechar compra
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}
