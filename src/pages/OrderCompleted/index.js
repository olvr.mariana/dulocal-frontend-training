import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'

import api from '../../services/api'
import './styles.css'

export default function OrderCompleted () {
  const login = useSelector((state) => state.login)
  const [creditBalance, setCreditBalance] = useState('')

  const orderData = useSelector((state) => state.order)
  const productsData = useSelector((state) => state.order[0].products)

  const total = useSelector((state) =>
    state.order[0].products.reduce((total, product) => {
      return total + product.value * product.amount
    }, 0)
  )

  const userToken = login[0]
  api
    .get('/user', { headers: { Authorization: userToken } })
    .then((response) => {
      setCreditBalance(response.data.wallet)
    })
    .catch((error) => {
      console.error(error)
    })

  return (
    <div className='container-purchase'>
      <div className='header-purchase'>
        {orderData.map((order) => (
          <div className='my-order' key={order.id}>
            <h3>Número do pedido</h3>
            <h6>{order.id}</h6>
          </div>
        ))}

        <div className='total'>
          <h3>Total Pago</h3>
          <h6>
            {Intl.NumberFormat('pt-br', {
              style: 'currency',
              currency: 'BRL'
            }).format(total)}
          </h6>
        </div>

        <div className='new-credit-balance'>
          <h3>Novo saldo</h3>
          <h6>
            {Intl.NumberFormat('pt-br', {
              style: 'currency',
              currency: 'BRL'
            }).format(creditBalance)}
          </h6>
        </div>
      </div>

      <div className='products-purchase'>
        <h2>Produtos comprados</h2>

        <div className='cards-purchase'>
          {productsData.map((product) => (
            <div className='cart-ordered-products' key={product.product_id}>
              <p>
                <strong>Número do produto: </strong> {product.product_id}
              </p>
              <p>
                <strong>Valor do produto: </strong>
                {Intl.NumberFormat('pt-br', {
                  style: 'currency',
                  currency: 'BRL'
                }).format(product.value)}
              </p>
              <p>
                <strong>Quantidade do produto: </strong> {product.amount}
              </p>
            </div>
          ))}
        </div>
      </div>
      <div className='btns-purchase'>
        <Link to='/pedidos'>
          <button className='btn btn-purchase'>Meus pedidos</button>
        </Link>

        <Link to='/lista-de-produtos'>
          <button className='btn btn-purchase'>Ver mais produtos</button>
        </Link>
      </div>
    </div>
  )
}
