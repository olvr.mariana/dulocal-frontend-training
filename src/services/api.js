import axios from 'axios'

export default axios.create({
  baseURL: 'https://trainning-storeapp-gateway-dev-g5kv7eofma-uw.a.run.app'
})
