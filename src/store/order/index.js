import { createAction, createReducer } from '@reduxjs/toolkit'

const INICIAL_STATE = []

export const createOrder = createAction('CREATE_ORDER')

export default createReducer(INICIAL_STATE, {
  [createOrder.type]: (state, action) => [action.payload]
})
