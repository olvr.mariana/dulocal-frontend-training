import { createAction, createReducer } from '@reduxjs/toolkit'
import produce from 'immer'

const INICIAL_STATE = []

export const addItem = createAction('ADD_ITEM')
export const removeItem = createAction('REMOVE_ITEM')
export const deleteAllFromCart = createAction('DELETE_ALL_FROM_CART')
export const clearCart = createAction('CLEAR_CART')

export default createReducer(INICIAL_STATE, {
  [addItem.type]: (state, action) =>
    produce(state, (draft) => {
      const productIndex = draft.findIndex((p) => p.id === action.payload.id)

      if (productIndex >= 0) {
        draft[productIndex].amount += 1
      } else {
        draft.push({
          ...action.payload,
          amount: 1
        })
      }
    }),

  [removeItem.type]: (state, action) =>
    produce(state, (draft) => {
      const productIndex = draft.findIndex((p) => p.id === action.payload)

      if (productIndex >= 0 && draft[productIndex].amount >= 2) {
        draft[productIndex].amount -= 1
      } else {
        draft.splice(productIndex, 1)
      }
    }),

  [deleteAllFromCart.type]: (state, action) =>
    state.filter((product) => product.id !== action.payload),

  [clearCart.type]: (state, action) => INICIAL_STATE
})
