import { createAction, createReducer } from '@reduxjs/toolkit'

const INICIAL_STATE = []

export const login = createAction('LOGIN')

export default createReducer(INICIAL_STATE, {
  [login.type]: (state, action) => [...state, action.payload]
})
