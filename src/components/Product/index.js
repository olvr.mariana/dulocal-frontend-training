import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { removeItem, addItem, deleteAllFromCart } from '../../store/cart'

import { FiTrash2, FiShoppingCart } from 'react-icons/fi'
import './styles.css'

export default function Product ({ product }) {
  const dispatch = useDispatch()

  const productQuantityCart = useSelector((state) =>
    state.cart.reduce((amount, product) => {
      amount[product.id] = product.amount
      return amount
    }, {})
  )

  function addItemCart (product) {
    dispatch(addItem(product))
  }

  function removeItemCart (id) {
    dispatch(removeItem(id))
  }

  function deleteAll (id) {
    dispatch(deleteAllFromCart(id))
  }

  return (
    <div className='container'>
      <div className='card-content'>
        <h4 className='product-name'>{product.name}</h4>
        <p>
          <strong>Valor: </strong>
          {Intl.NumberFormat('pt-br', {
            style: 'currency',
            currency: 'BRL'
          }).format(product.value)}
        </p>

        <div className='infos'>
          <div className='units'>
            <p>
              <strong>Quantidade: </strong>
              {productQuantityCart[product.id] || 0}
            </p>

            <p>
              <strong>Subtotal: </strong>

              {Intl.NumberFormat('pt-br', {
                style: 'currency',
                currency: 'BRL'
              }).format(productQuantityCart[product.id] * product.value || 0)}
            </p>
          </div>
          <div className='btns-units'>
            <button
              className='btn btn-units'
              onClick={() => addItemCart(product)}
            >
              <strong>
                + <FiShoppingCart />
              </strong>
            </button>
            <button
              className='btn btn-units'
              onClick={() => removeItemCart(product.id)}
            >
              <strong>
                - <FiShoppingCart />
              </strong>
            </button>
            <button
              className='btn btn-units'
              onClick={() => deleteAll(product.id)}
            >
              <FiTrash2 />
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}
