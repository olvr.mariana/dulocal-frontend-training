describe('My First Teste', function () {
    it('Enter localhost', function () {
        cy.visit('localhost:3000')

        cy.pause()

        cy.contains('Já sou cliente, quero entrar!').click()

        cy.url().should('include', '/login')

        cy.get('#user-email')
        .type('cliente@cliente.com')
        .should('have.value', 'cliente@cliente.com')
            
    })

})